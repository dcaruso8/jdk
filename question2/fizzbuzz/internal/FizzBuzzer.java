import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;
import java.util.logging.Logger;

public class FizzBuzzer
{
    private String fizzBuzzWords;
    private List<Function<Integer, String>> fizzBuzzFuncs;
    private static Logger log = Logger.getLogger(FizzBuzzer.class.getName());

    public FizzBuzzer()
    {
        fizzBuzzFuncs = null;
//        log.addHandler(new ConsoleHandler());
    }

    public void addFizzBuzz(Function<Integer, String> fn)
    {
        log.info("Adding filter to FizzBuzz : " + fn.toString());
        if (fizzBuzzFuncs == null)
        {
            fizzBuzzFuncs = new ArrayList<Function<Integer,String>>();
        }
        fizzBuzzFuncs.add(fn);
    }

    public String feed(int v)
    {
        String fizzBuzzWords = "";
        for(Function<Integer, String> fn : fizzBuzzFuncs)
        {
            fizzBuzzWords = fizzBuzzWords.concat(fn.apply(v));
        }
        if (fizzBuzzWords.equals(""))
        {
            fizzBuzzWords = String.valueOf(v);
        }

        return fizzBuzzWords;
    }

}
