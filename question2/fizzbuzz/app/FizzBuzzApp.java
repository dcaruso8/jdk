public class FizzBuzzApp
{
    public static void main(String[] args)
    {
        FizzBuzzer fbz = new FizzBuzzer();

        fbz.addFizzBuzz(x -> (x % 3) == 0 ? "Fizz" : "");
        fbz.addFizzBuzz(x -> (x % 7) == 0 ? "Buzz" : "");
//        fbz.addFizzBuzz(x -> ((x*x) % 2) == 0 ? "- Kamoulox -" : "");

        for (int i=1; i<100; i++)
        {
            System.out.println(fbz.feed(i));
        }
        System.exit(1);
    }
}
